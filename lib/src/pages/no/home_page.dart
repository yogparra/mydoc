import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Creación de carpeta'),
        elevation: 0,
      ),
      body: const Center(
        child: Text('Home'),
      ),
    );
  }
}
