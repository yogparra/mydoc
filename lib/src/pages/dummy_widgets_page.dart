import 'package:flutter/material.dart';
//import 'package:mydoc/src/pages/get_started_page.dart';
import 'package:mydoc/src/pages/sing_in.dart';

class DummyWidgets extends StatelessWidget {
  const DummyWidgets({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: const <Widget>[
            //GetStarted()
            SingIn()
        ],
      ),
    );
  }
}