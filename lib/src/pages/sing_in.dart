import 'package:flutter/material.dart';

class SingIn extends StatelessWidget {
  const SingIn({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: SizedBox(
        child: Column(
          children: <Widget>[
            _Titulo(),            
            const SizedBox( height: 30 ),
            _CajaUno(),
            const SizedBox( height: 10 ),
            _CajaDos(),            
            const SizedBox( height: 5 ),
           _CajaCheck(),
            _BotonGordoConColor(),
            _BotonGordoSinColor(),
            _TituloPequeno()
          ],
        ),
      ),
    );
  }
}

class _Titulo extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(top:25),
      child: SizedBox(        
        child: Text('Sing in', style: TextStyle( fontSize: 25, fontWeight: FontWeight.w600))
      ),
    );
  }
}

class _BotonGordoConColor extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:100),        
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(            
            width: 200, 
            child: TextButton(
              onPressed: () {},
              style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(Colors.white),
                  backgroundColor: MaterialStateProperty.all(const Color(0xffF50057)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                      side: const BorderSide(color: Color(0xffF50057))
                    )
                  )
              ),
              child: const Text('Sing In')
              ),
          )
        ],
      ),
    );
  }
}

class _TituloPequeno extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(top:25),
      child: SizedBox(
        child: Text('Dont have Account?', style: TextStyle( fontSize: 12, fontWeight: FontWeight.w600))
      ),
    );
  }
}

class _BotonGordoSinColor extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 200,
            child: TextButton(
              onPressed: () {},
              style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(const Color(0xffF50057)),
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                      side: const BorderSide(color: Color(0xffF50057))
                    )
                  )
              ),
              child: const Text('Sing In')
              ),
          )
        ],
      ),
    );
  }
}

class _CajaUno extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return TextFormField(
          decoration: const InputDecoration(
          hintText: 'john.doe@gmail.com',
          prefixIcon: Icon(Icons.person_2_outlined, color: Color(0xffF50057)),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xffF50057)
            )
          ),

          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xffF50057),
              width: 1,              
            )
          ),

        ),
    );
  }
}

class _CajaDos extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        
        decoration: const InputDecoration(
          hintText: '*******',
          prefixIcon: Icon(Icons.lock_outlined, color: Color(0xffF50057)),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xffF50057)
            )
          ),

          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xffF50057),
              width: 1,
            )
          ),

        ),
    );
  }
}

class _CajaCheck extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          activeColor: const Color(0xffF50057),
          value: true,
          onChanged: (value) {},
        ),
        const Text('Remenber', style: TextStyle( fontSize: 12, fontWeight: FontWeight.w600)),
        const SizedBox( width: 75, ),
        const Text('Forget Password?', style: TextStyle( fontSize: 12, fontWeight: FontWeight.w600)),
        ],
      ); 
    }
}

/*
  CheckboxListTile(
      title: const Text("title text"),
      value: true,
      onChanged: (value) {},                  
  ),

*/