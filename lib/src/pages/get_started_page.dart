import 'package:flutter/material.dart';

class GetStarted extends StatelessWidget {
  const GetStarted({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: SizedBox(
        child: Column(
          children: <Widget>[
              _Titulo(),
              _Imagen(),
              const Text('Lorem ipsum dolor sit amet, consectetur', style: TextStyle(  fontSize: 15, fontWeight: FontWeight.w600)),
              _BotonGordo(),
              _TituloPequeno(),
              _TituloFinal()
          ],
        ),
      ),
    );
  }
}

class _Titulo extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(top:25),
      child: SizedBox(        
        child: Text('Hello!', style: TextStyle( fontSize: 25, fontWeight: FontWeight.w600))
      ),
    );
  }
}

class _Imagen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      child: Image(image: AssetImage('assets/imgs/rojo.png'))
    );
  }
}

class _BotonGordo extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:100),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 200,
            child: TextButton(
              onPressed: () {},
              style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(Colors.white),
                  backgroundColor: MaterialStateProperty.all(const Color(0xffF50057)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                      side: const BorderSide(color: Color(0xffF50057))
                    )
                  )
              ),
              child: const Text('GET STARTED')
              ),
          )
        ],
      ),
    );
  }
}

class _TituloPequeno extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(top:25),
      child: SizedBox(
        child: Text('Dont have an account?', style: TextStyle( fontSize: 12, fontWeight: FontWeight.w600))
      ),
    );
  }
}

class _TituloFinal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(top:5),
      child: SizedBox(
        child: Text('Sing in here', style: TextStyle(
          fontSize: 25,
          fontWeight: FontWeight.w600,
          decoration: TextDecoration.underline
          ))
      ),
    );
  }
}