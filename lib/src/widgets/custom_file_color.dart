import 'package:flutter/material.dart';

class CustomFileColor extends StatelessWidget {
  const CustomFileColor({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 5),
      child: Container(
        width: double.infinity,
        height: 430,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(50),
          boxShadow: const [          
            BoxShadow( 
                color: Colors.black12, 
                blurRadius: 15, 
                offset: Offset(0,7)
              )
        ]
        ),
        child: Column(
          children: <Widget>[

              _File(),
              _ColorFile()

          ],
        ),
      ),
    );
  }
}

class _File extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Stack(
        children: const <Widget>[
              Image(image: AssetImage('assets/imgs/amarillo.png'))
        ],
      ),
    );
  }
}

class _ColorFile extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric( horizontal: 80),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: const <Widget>[
    
          _Color('Ro'),
          _Color('Am'),
          _Color('Mo'),
          _Color('Ve')
    
        ],
      ),
    );
  }
}

class _Color extends StatelessWidget {
  
  final String texto;
  const _Color( this.texto );

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: 45,
      height: 45,      
      decoration: BoxDecoration(
        color: ( texto == 'Am' ) ? const Color(0xffF9A826) : Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          if ( texto == 'Am')
            const BoxShadow( 
                color: Colors.black, 
                blurRadius: 10, 
                offset: Offset(0,5)
              )
        ]
      ),
      child: Text(texto,
        style: const TextStyle(
          color: Colors.black,
          fontSize: 16,
          fontWeight: FontWeight.bold
        ),
      ),
    );
  }
}