# My doc: App para la gestion de carpetas de arranque

### Repositorio
```
    https://gitlab.com/yogparra/mydoc
```

### Trello
```
    https://trello.com/b/baCphY5C/monki
```

### Version
```
    ----------------------
        Flutter 3.0.5
        Dart 2.17.6
        DevTools 2.12.2
    ----------------------

    ----------------------
        Flutter 3.7.7
        Dart 2.19.4
        DevTools 2.20.1
    ----------------------    
```

### Recursos
```
    https://undraw.co/ color (#6C63FF, #6C63FF)
    https://www.uplabs.com/            
```

### Paquetes
```
    https://pub.dev/packages        
```

### Inicio del proyecto
```
    flutter pub get
```

### Estructura del Proyecto
```
    /lib
        /src
            /models
            /pages
                home
                pages
                carpeta
                formulario
                doc_empresa
                seleccion_trabajadores
                trabajadores
                dummy_widgets
            /widgets
                custom_appbar
                custom_file_color
                custom_widgets
        main.dart
```


### Mock
```
    carpeta
    formulario
    doc_empresa
    seleccion_trabajadores
    trabajadores
```

### Trucos
```
    ctrl.
    stl.  ==> StatelessWidget
    imporm
```

### Colores
```
    #6C63FF.- morado
    #F50057.- rojo
    #F9A826.- amarillo
```

### Investigar
```
    Tabnine
```

